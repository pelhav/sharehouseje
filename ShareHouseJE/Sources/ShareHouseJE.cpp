﻿// ShareHouseJE.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//

#include "pch.h"
#include "framework.h"
#include "ShareHouseJE.h"

#include <memory>

#include "Platform/Window/Application.h"
#include "Debug/Log.h"
#include "Render/D3D11/Device.h"


using namespace Platform;


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                      _In_opt_ HINSTANCE hPrevInstance,
                      _In_ LPWSTR lpCmdLine,
                      _In_ int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	UNREFERENCED_PARAMETER(nCmdShow);

	Log::Initialize();
	
	const auto pDevice = Render::D3D11::Device::Create();
	if(pDevice == nullptr)
	{
		return -1;
	}

	//윈도우 생성 및 실행
	const auto windowAppInfo = Window::AppInfo
	{
		hInstance,
		L"style_sh_je",
		L"ShareHouseJE",
		800,
		600
	};

	auto pWindowApp = std::unique_ptr<Window::Application>(Window::Application::Create(windowAppInfo));
	if (pWindowApp == nullptr)
	{
		return -1;
	}

	pDevice->CreateSwapChain(pWindowApp->GetWindowHandle(), DXGI_FORMAT_R8G8B8A8_UNORM);
	
	LOG_INFO("Hello, Engine");
	pWindowApp->Run();

	delete pDevice;

	return 0;
}
