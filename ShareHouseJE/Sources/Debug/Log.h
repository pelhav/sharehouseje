#pragma once

#include <spdlog/spdlog.h>

class Log
{
public:
	static void Initialize();
	static void StackTrace(const TCHAR* fileName, const TCHAR* funcName, const int nLine);
};

#define LOG_INFO(...) spdlog::info(__VA_ARGS__); Log::StackTrace(__FILEW__, __FUNCTIONW__, __LINE__)
#define LOG_ERROR(...) spdlog::error(__VA_ARGS__); Log::StackTrace(__FILEW__, __FUNCTIONW__, __LINE__)
