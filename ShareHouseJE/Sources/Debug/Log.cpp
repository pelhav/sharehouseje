#include "pch.h"
#include "Log.h"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/msvc_sink.h>
#include "Config/Environment.h"
#include <boost/format.hpp>


void Log::Initialize()
{
	auto sink = std::make_shared<spdlog::sinks::msvc_sink_mt>();
	auto logger = std::make_shared<spdlog::logger>("", sink);
	spdlog::set_default_logger(logger);

	if(Environment::IsDebug() == false)
	{
		spdlog::set_level(spdlog::level::off);
	}
}

void Log::StackTrace(const TCHAR* fileName, const TCHAR* funcName, const int nLine)
{
	const auto format = boost::wformat(L" - %1% Line:%3%\n - %2%()\n\n") % fileName % funcName % nLine;
	const auto str = boost::io::str(format);
	OutputDebugString(str.c_str());
}
