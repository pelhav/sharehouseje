#pragma once

class Environment
{
public:
	static bool IsDebug()
	{
		return
#ifdef _DEBUG
			true;
#else
			false;
#endif
	}
};
