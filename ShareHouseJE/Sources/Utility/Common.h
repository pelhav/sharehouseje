#pragma once

template <typename T>
inline void SafeDelete(T*& pRef)
{
	if(pRef == nullptr)
	{
		return;
	}
	
	delete pRef;
	pRef = nullptr;
};


template <typename T>
inline void SafeDeleteArray(T*& pRef)
{
	if(pRef == nullptr)
	{
		return;
	}
	
	delete[] pRef;
	pRef = nullptr;
}

#define IF_EXIST(pointer) if(pointer) pointer