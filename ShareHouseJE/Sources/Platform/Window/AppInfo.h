#pragma once

#include <Windows.h>

namespace Platform
{
	namespace Window
	{
		struct AppInfo
		{
			const HINSTANCE hInstance;
			const WCHAR* szWindowClass;
			const WCHAR* szTitle;
			const int width;
			const int height;
		};
	}
}
