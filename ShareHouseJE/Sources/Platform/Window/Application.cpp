#include "pch.h"
#include "Resource.h"
#include "Application.h"

#include <iostream>
#include "Debug/Log.h"

using namespace Platform::Window;


LRESULT CALLBACK Application::OnReceiveMessage(const HWND hWnd, const UINT message, const WPARAM wParam,
                                                     const LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

Application* Application::Create(AppInfo info)
{
	const auto pApp = new Application(info);
	if(pApp->_hWnd == nullptr)
	{
		delete pApp;
		return nullptr;
	}
	
	return pApp;
}

Application::Application(AppInfo info)
	:_info(info)
{
	WNDCLASSEXW winClassData;

	winClassData.cbSize = sizeof(WNDCLASSEX);

	winClassData.style = CS_OWNDC; // | CS_HREDRAW | CS_VREDRAW;
	winClassData.lpfnWndProc = OnReceiveMessage;
	winClassData.cbClsExtra = 0;
	winClassData.cbWndExtra = 0;
	winClassData.hInstance = _info.hInstance;
	winClassData.hIcon = LoadIcon(_info.hInstance, MAKEINTRESOURCE(IDI_SHAREHOUSEJE));
	winClassData.hCursor = LoadCursor(nullptr, IDC_ARROW);
	winClassData.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	winClassData.lpszMenuName = nullptr;
	winClassData.lpszClassName = _info.szWindowClass;
	winClassData.hIconSm = LoadIcon(winClassData.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	if (RegisterClassEx(&winClassData) == false)
	{
		LOG_ERROR(L"Class registration has failed!");
		return;
	}

	_hWnd = CreateWindow(_info.szWindowClass, _info.szTitle, WS_OVERLAPPEDWINDOW,
	                      CW_USEDEFAULT, 0, _info.width, _info.height, nullptr, nullptr, _info.hInstance, nullptr);

	if (_hWnd == nullptr)
	{
		LOG_ERROR(L"CreateWindow() failed!");
		return;
	}

	ShowWindow(_hWnd, SW_SHOW);
	SetForegroundWindow(_hWnd);
	SetFocus(_hWnd);

	bool isFullScreen = false;

	if(isFullScreen)
	{
		
	}
	else
	{

	}
}

Application::~Application()
{
	if (_hWnd)
	{
		DestroyWindow(_hWnd);
		_hWnd = nullptr;
	}

	UnregisterClass(_info.szWindowClass, _info.hInstance);
}

// ReSharper disable once CppMemberFunctionMayBeStatic
int Application::Run()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if (Update() == false)
			{
				break;
			}
		}
	}

	return static_cast<int>(msg.wParam);
}

bool Application::Update()
{
	//esc 감지
	//렌더러 - 엔진에 업데이트 제공

	return true;
}
