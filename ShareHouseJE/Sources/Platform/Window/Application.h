#pragma once

#include <Windows.h>
#include "AppInfo.h"


//����: https://copynull.tistory.com/238?category=649932

namespace Platform
{
	namespace Window
	{
		class Application
		{
		private:
			static LRESULT CALLBACK OnReceiveMessage(HWND, UINT, WPARAM, LPARAM);
		public:
			static Application* Create(AppInfo info);

		private:
			const AppInfo _info;
			HWND _hWnd = nullptr;

		private:
			explicit Application(AppInfo info);
		public:
			~Application();

			int Run();
			bool Update();

			HWND GetWindowHandle() const { return _hWnd; }
		};
	}
}
