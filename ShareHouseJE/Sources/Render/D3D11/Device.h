#pragma once

#include <d3d11.h>
#include "Utility/ComPtr.h"

namespace Render
{
	namespace D3D11
	{
		class Device
		{
		public:
			static Device* Create();

		private:
			ComPtr<ID3D11Device> _pDevice;
			ComPtr<ID3D11DeviceContext> _pContext;
			D3D_FEATURE_LEVEL _featureLevel = D3D_FEATURE_LEVEL_1_0_CORE;
			bool _isInitialized = false;

			DXGI_FORMAT _renderTargetFormat = DXGI_FORMAT_UNKNOWN;
			ComPtr<IDXGISwapChain> _pSwapChain;

		private:
			Device();
		public:
			bool CreateSwapChain(HWND hwnd, DXGI_FORMAT bufferFormat);
		};
	}
}
