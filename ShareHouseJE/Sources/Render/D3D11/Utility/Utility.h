#pragma once

#include <d3d11.h>

//TODO: 해당 함수 호출을 특정 타입으로 제한할 수 있는 방법을 찾아야 함.
template <class T = IUnknown>
inline void SafeRelease(T*& pRef)
{
	if (pRef == nullptr)
	{
		return;
	}

	pRef->Release();
	pRef = nullptr;
}

template <class T = IUnknown>
inline const IID& UuidOf()
{
	return __uuidof(T);
}

template <class T = IUnknown>
inline void** ToVoidAddress(T** pointer)
{
	return reinterpret_cast<void**>(pointer);
}

class ScopeComRelease
{
	IUnknown* _pCom = nullptr;

public:
	ScopeComRelease(IUnknown* pCom)
		: _pCom(pCom)
	{
	}

	~ScopeComRelease()
	{
		if (_pCom)
		{
			_pCom->Release();
		}
	}
};
