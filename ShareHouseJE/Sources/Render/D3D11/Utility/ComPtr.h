#pragma once

#include <d3d11.h>
#include "Utility.h"

template <class T = IUnknown>
class ComPtr
{
	T* _pCom = nullptr;

public:
	ComPtr() = default;

	ComPtr(T* pointer)
		: _pCom(pointer)
	{
	}

	ComPtr(const ComPtr& other)
		: _pCom(other._pCom)
	{
		if (_pCom)
		{
			_pCom->AddRef();
		}
	}

	ComPtr(ComPtr&& other) noexcept
		: _pCom(other._pCom)
	{
		if (_pCom)
		{
			_pCom->AddRef();
		}
	}

	~ComPtr()
	{
		SafeRelease(_pCom);
	}

	T* GetAddress() { return _pCom; }
	T** GetReceivableAddress() { return &_pCom; }
	void** GetVoidReceivableAddress() { return ToVoidAddress(&_pCom); }

	ComPtr& operator=(T*& pCom)
	{
		_pCom = pCom;
		return *this;
	}

	operator T*() const { return _pCom; }
	T* operator->() const { return _pCom; }
	operator bool() const { return _pCom != nullptr; }

	ComPtr& operator=(const ComPtr& other)
	{
		if (this == &other)
		{
			return *this;
		}

		if (_pCom)
		{
			_pCom->Release();
		}

		_pCom = other._pCom;
		if (_pCom)
		{
			_pCom->AddRef();
		}
		return *this;
	}

	ComPtr& operator=(ComPtr&& rhs) noexcept
	{
		if (_pCom)
		{
			_pCom->Release();
		}

		_pCom = rhs._pCom;
		if (_pCom)
		{
			_pCom->AddRef();
		}
		return *this;
	}
};
