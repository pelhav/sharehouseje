#include "pch.h"

#include "Device.h"
#include "Config/Environment.h"
#include "Debug/Log.h"
#include "Utility/Utility.h"
#include "Utility/ComPtr.h"
#include "Utility/Common.h"

using namespace Render::D3D11;

Device* Device::Create()
{
	const auto pDevice = new Device();
	if (pDevice->_isInitialized == false)
	{
		delete pDevice;
		return nullptr;
	}

	return pDevice;
}

Device::Device()
{
	UINT flags = 0;
	if (Environment::IsDebug())
	{
		flags |= D3D11_CREATE_DEVICE_DEBUG;
	}
	//flags |= D3D11_CREATE_DEVICE_SINGLETHREAEDED

	const HRESULT result =
		D3D11CreateDevice(nullptr,
		                  D3D_DRIVER_TYPE_HARDWARE,
		                  nullptr,
		                  flags,
		                  nullptr,
		                  0,
		                  D3D11_SDK_VERSION,
		                  _pDevice.GetReceivableAddress(),
		                  &_featureLevel,
		                  _pContext.GetReceivableAddress()
		);

	if (FAILED(result))
	{
		LOG_ERROR(L"D3D11CreateDevice() Failed.");
		return;
	}

	if (_featureLevel < D3D_FEATURE_LEVEL_11_0)
	{
		LOG_ERROR(L"D3D Feature Level is too low.");
		return;
	}

	LOG_INFO(L"D3D11CreateDevice() Success!");
	_isInitialized = true;
}

bool Device::CreateSwapChain(HWND hwnd, DXGI_FORMAT bufferFormat)
{
	_renderTargetFormat = bufferFormat;
	//윈도우 모드에서 윈도우 내부 영역을 구해서 스왑체인을 생성

	// RECT r;
	// GetClientRect(hwnd, &r);
	// int width = r.right - r.left;
	// int height = r.bottom - r.top;
	//
	// LOG_INFO(L"Actual Size: Width:{}, Height:{}", width, height);

	DXGI_MODE_DESC bufferDesc;
	bufferDesc.Width = 0;
	bufferDesc.Height = 0; //0일시 크기 자동
	//사용자 설정에 따라 다르게
	bufferDesc.RefreshRate.Numerator = 60; //TODO: 해당 디바이스의 최대 프레임레이트로 교체?
	bufferDesc.RefreshRate.Denominator = 1;
	bufferDesc.Format = _renderTargetFormat;
	//인터레이스-프로그레시브 설정 가능
	bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	//풀스크린일시 스케일링 모드 설정 (ex: 4:3 비율 스타크래프트)
	//아마 윈도우모드는 상관없을듯?
	bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	//--MSAA 설정은 패스, 추후 FXAA 구현하는 쪽으로
	// * AA끼리 중첩가능하긴 함.
	DXGI_SAMPLE_DESC sampleDesc;
	sampleDesc.Count = 1;
	sampleDesc.Quality = 0;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	swapChainDesc.BufferDesc = bufferDesc;
	swapChainDesc.SampleDesc = sampleDesc;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 1; // double buffering = 1, triple = 2 ...
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.Windowed = true;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = 0;

	ComPtr<IDXGIDevice> pGiDevice;
	ComPtr<IDXGIAdapter> pGiAdapter;
	ComPtr<IDXGIFactory> pGiFactory;

	_pDevice->QueryInterface(pGiDevice.GetReceivableAddress());
	
	IF_EXIST(pGiDevice)->GetParent(UuidOf<IDXGIAdapter>(), pGiAdapter.GetVoidReceivableAddress());
	IF_EXIST(pGiAdapter)->GetParent(UuidOf<IDXGIFactory>(), pGiFactory.GetVoidReceivableAddress());
	//TODO: IDXGIFactory2::CreateSwapChainForHwnd() 로 교체해야함.
	//- 현재는 MSDN 읽어보면서 인터페이스 교체할 시간이 없으니 일단 패스
	IF_EXIST(pGiFactory)->CreateSwapChain(_pDevice, &swapChainDesc, _pSwapChain.GetReceivableAddress());
	
	
	return _pSwapChain != nullptr;
}
